const levels = 15

generateRandomKey = () => {
  const min = 65
  const max = 90
  return Math.round(Math.random() * (max - min) + min)
}

generateKeys = () => new Array(levels).fill(0).map(generateRandomKey)

let keys = generateKeys()

getElementByKeyCode = (keyCode) => document.querySelector(`[data-key="${keyCode}"]`)

deactivate = (el) => el.className = 'key'

activate = (keyCode, opts = {}) => {
  const el = getElementByKeyCode(keyCode)
  el.classList.add('active')
  if (opts.success) {
    el.classList.add('success')
  } else if (opts.fail) {
    el.classList.add('fail')
  }
  setTimeout(() => deactivate(el), 500)
}

nextLevel = (currentLevel) => {
  if (currentLevel == levels) {
    return swal({
      title: 'You win :)',
      icon: 'success'
    })
  }

  swal({
    title: `Level ${currentLevel + 1}`,
    timer: 1000,
    button: false
  })

  for (let i = 0; i <= currentLevel; i++) {
    setTimeout(() => activate(keys[i]), 1500 + 1000 * i)
  }

  let i = 0
  let currentKey = keys[i]
  window.addEventListener('keydown', onkeydown)

  function onkeydown(ev) {
    if (ev.keyCode == currentKey) {
      activate(currentKey, { success: true })
      i++
      if (i > currentLevel) {
        window.removeEventListener('keydown', onkeydown)
        setTimeout(() => nextLevel(i), 1500)
      }
      currentKey = keys[i]
    } else {
      activate(ev.keyCode, { fail: true })
      window.removeEventListener('keydown', onkeydown)
      setTimeout(() => swal({
        title: 'You lose :(',
        text: 'Wanna play again?',
        icon: "warning",
        buttons: true,
        dangerMode: true
      }).then((response) => {
        if(response) {
          keys = generateKeys()
          nextLevel(0)
        }
      }), 500)
    }
  }
}

nextLevel(0)

[![pipeline status](https://gitlab.com/bardux/simon-dice/badges/master/pipeline.svg)](https://gitlab.com/bardux/simon-dice/commits/master)

# Simón dice

Juego en JavaScript

## Descripción del juego

- El juego cuenta con 15 níveles
- El jugador debe seguir las teclas en el mismo órden

## Online
[https://bardux.gitlab.io/simon-dice](https://bardux.gitlab.io/simon-dice)

## Créditos
- [Victor Lozano](https://twitter.com/imbardux)

## Licencia
[MIT](https://opensource.org/licenses/MIT)
